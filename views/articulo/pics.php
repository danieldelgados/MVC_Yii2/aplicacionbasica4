<?php
use yii\helpers\Html
?>

<div class="row">
<?php
foreach($fotosArticulo as $foto){
 ?>   
<div class="col-xs-6 col-md-3">
    <a href="#" class="thumbnail">
      <?= Html::img('@web/imgs/' . $foto->nombre, ['alt' => 'My logo']) ?>
    </a>
  </div>
<?php
}
?>  
</div>