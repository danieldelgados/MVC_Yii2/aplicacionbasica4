<?php
/* @var $this yii\web\View */
$this->title = 'listado';

use yii\helpers\Html;
?>
<div class="site-index">
    <nav aria-label="Page navigation">
        <ul class="pagination">
            <li>
                <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <?php
            foreach ($articulos as $paginas) {
                ?>
                <li><?= Html::a($paginas->id, ['site/index'], ['class' => 'profile-link']) ?></li>
                <?php
            }
            ?>
            <li>
                <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>

    <?php
    foreach ($articulos as $articulo) {
        ?>    
        <div class="row">
            <?php
            echo $this->render("index/_articulo", [
                "datos" => $articulo,
            ]);

            /**
             * del articulo actual quiero ver las fotos
             */
            $fotosArticulo = $articulo->fotos; // esto es un array de modelos Foto

            foreach ($fotosArticulo as $foto) {
                echo $this->render("index/_fotos", [
                    "datos" => $foto,
                ]);
            }
            ?> 
        </div>
        <?php
    }
    ?>       


</div>
