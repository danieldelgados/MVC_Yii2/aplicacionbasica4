<?php
 use yii\helpers\Html;
?>

<ul class="nav nav-pills nav-stacked">
    <li role="presentation"><?=Html::a('Articulos',['articulo/index'], ['class' => 'botones btn btn-info']); ?></li>
    <li role="presentation"><?=Html::a('Fotos',['foto/index'], ['class' => 'botones btn btn-info']); ?></li> 
</ul>
